ansible-splunkforwarder
=========

Ansible role for downloading and installing Splunk Universal Forwarder on Debian, Ubuntu, CentOS or RedHat Enterprise Linux

Role Variables
--------------

- `splunk_version`: Splunk version number. Defaults to 6.4.2
- `splunk_build`: The build ID for the given version number. For 6.4.2 this is "00f5bb3fa822"

If you want to install a different version, change these variables. To find the build id's and older versions head over to splunk.com: https://www.splunk.com/page/previous_releases

Example Playbook
----------------

Just look at tests/test.yml

License
-------

MIT

Author Information
------------------

Trond Sjøvang, https://trond.sjovang.no
